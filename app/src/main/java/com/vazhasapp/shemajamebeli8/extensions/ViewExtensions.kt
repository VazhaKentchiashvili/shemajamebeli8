package com.vazhasapp.shemajamebeli8.extensions

import android.graphics.Color
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.vazhasapp.shemajamebeli8.R

// Bottom Nav Badge Setup
fun BottomNavigationView.setupBadge(menuItemId: Int, badgeNumber: Int) {
    getOrCreateBadge(menuItemId).badgeTextColor = Color.WHITE
    getOrCreateBadge(menuItemId).number = badgeNumber
    getOrCreateBadge(menuItemId).backgroundColor = Color.RED
    getOrCreateBadge(menuItemId).isVisible = true
}

fun ImageView.setupIcon(iconUrl: String) {
    Glide.with(context).load(iconUrl).placeholder(R.drawable.ic_not_found)
        .into(this)
}