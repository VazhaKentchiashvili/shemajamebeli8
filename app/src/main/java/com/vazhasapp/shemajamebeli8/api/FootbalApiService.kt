package com.vazhasapp.shemajamebeli8.api

import com.vazhasapp.shemajamebeli8.utils.Constants.BASE_URL
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object FootbalApiService {

    private val retrofitBuilder: Retrofit by lazy {
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    val footballApiService: FootballApi by lazy {
        retrofitBuilder.create(FootballApi::class.java)
    }
}