package com.vazhasapp.shemajamebeli8.api

import com.vazhasapp.shemajamebeli8.model.Football
import com.vazhasapp.shemajamebeli8.utils.Constants.API_ENDPOINT
import retrofit2.Response
import retrofit2.http.GET

interface FootballApi {

    @GET(API_ENDPOINT)
    suspend fun footballResult() : Response<Football>
}