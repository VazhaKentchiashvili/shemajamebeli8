package com.vazhasapp.shemajamebeli8.adapters

enum class GoalTypes(var goalTypes: Int) {
    GOAL(1),
    OWN_GOAL(2),
}