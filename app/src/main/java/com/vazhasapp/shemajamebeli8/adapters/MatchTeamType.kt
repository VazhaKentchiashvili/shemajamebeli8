package com.vazhasapp.shemajamebeli8.adapters

enum class MatchTeamType(var teamType: Int) {
    TEAM1(1),
    TEAM2(2),
}