package com.vazhasapp.shemajamebeli8.adapters

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.vazhasapp.shemajamebeli8.R
import com.vazhasapp.shemajamebeli8.databinding.Team1ActionCardViewBinding
import com.vazhasapp.shemajamebeli8.extensions.setupIcon
import com.vazhasapp.shemajamebeli8.model.Summary
import com.vazhasapp.shemajamebeli8.model.TeamAction

class TeamFirstRecycler(private val firstTeamActions: Summary) :
    RecyclerView.Adapter<TeamFirstRecycler.TeamFirstViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        TeamFirstViewHolder(
            Team1ActionCardViewBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: TeamFirstViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount() = firstTeamActions.team1Action?.size ?: 0

    inner class TeamFirstViewHolder(private val binding: Team1ActionCardViewBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind() {
            var firstTeam: TeamAction? = firstTeamActions.team1Action?.get(adapterPosition)

            binding.tvTeam1PlayerTitle.text = firstTeam?.action?.player1?.playerName
            binding.imTeam1PlayerIcon.setupIcon(firstTeam?.action?.player1?.playerImage.toString())

            when (firstTeam?.actionType) {
                ActionsTypes.GOAL.actionsTypes -> {
                    when (firstTeam.action?.goalType) {
                        GoalTypes.GOAL.goalTypes -> {
                            binding.imTeam1ActionIcon.setImageResource(R.drawable.ic_goal)
                            binding.tvTeam1ActionTime.text = firstTeamActions.actionTime
                            binding.tvTeam1ActionType.text = "${R.string.goals_by}"
                            binding.tvTeam1ActionType.text = "` Goals by"
                        }
                        GoalTypes.OWN_GOAL.goalTypes -> {
                            binding.imTeam1ActionIcon.setImageResource(R.drawable.ic_own_goal)
                            binding.tvTeam1ActionTime.text = firstTeamActions.actionTime
                            binding.tvTeam1ActionTime.setTextColor(Color.RED)
                            binding.tvTeam1ActionType.text = "` Own Goal"
                            binding.tvTeam1ActionType.setTextColor(Color.RED)
                        }
                    }
                }

                ActionsTypes.YELLOW_CARD.actionsTypes -> {
                    binding.imTeam1ActionIcon.setImageResource(R.drawable.ic_yellow_card)
                    binding.tvTeam1ActionTime.text = firstTeamActions.actionTime
                    binding.tvTeam1ActionType.text = "` Tripping"
                }

                ActionsTypes.RED_CARD.actionsTypes -> {
                    binding.imTeam1ActionIcon.setImageResource(R.drawable.ic_yellow_card)
                    binding.imTeam1ActionIcon.setColorFilter(Color.RED)
                    binding.tvTeam1ActionTime.text = firstTeamActions.actionTime
                    binding.tvTeam1ActionType.text = "` Tripping"
                }

                ActionsTypes.SUBSTITUTION.actionsTypes -> {
                    binding.imTeam1ActionIcon.setImageResource(R.drawable.ic_substitution_in)
                    binding.imTeam1ActionIcon2.setImageResource(R.drawable.ic_substitutin_out)
                    binding.tvTeam1ActionTime.text = firstTeamActions.actionTime
                    binding.tvTeam1ActionType.text = "` Substitution"
                    binding.tvTeam1PlayerTitle2.text = firstTeam.action?.player2?.playerName
                    binding.tvTeam1PlayerTitle2.visibility = View.VISIBLE
                    binding.imTeam1PlayerIcon2.visibility = View.VISIBLE
                    binding.imTeam1PlayerIcon2.setImageResource(R.drawable.ic_not_found)
                }
            }
        }
    }
}
