package com.vazhasapp.shemajamebeli8.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.vazhasapp.shemajamebeli8.R
import com.vazhasapp.shemajamebeli8.databinding.ActionsViewBinding
import com.vazhasapp.shemajamebeli8.model.Summary

class MainRecyclerView : RecyclerView.Adapter<MainRecyclerView.MainRecyclerViewHolder>() {

    private val actionsList = mutableListOf<Summary>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        MainRecyclerViewHolder(
            ActionsViewBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: MainRecyclerViewHolder, position: Int) {

        holder.bind()

        setTeamFirstRecycler(holder.firstTeamRv, actionsList[position])

        setTeamSecondRecycler(holder.secondTeamRv, actionsList[position])
    }

    override fun getItemCount() = actionsList.size

    inner class MainRecyclerViewHolder(private val binding: ActionsViewBinding) :
        RecyclerView.ViewHolder(binding.root) {

        lateinit var firstTeamRv: RecyclerView
        lateinit var secondTeamRv: RecyclerView

        lateinit var currentAction: List<Summary>

        fun bind() {
            currentAction = actionsList
            firstTeamRv = binding.rvTeam1
            secondTeamRv = binding.rvTeam2
        }
    }

    private fun setTeamFirstRecycler(
        teamFirstRecycler: RecyclerView,
        eachActionList: Summary
    ) {
        val teamFirstAdapter = TeamFirstRecycler(eachActionList)
        teamFirstRecycler.adapter = teamFirstAdapter
        teamFirstRecycler.layoutManager = LinearLayoutManager(teamFirstRecycler.context)
        teamFirstRecycler.setHasFixedSize(true)
    }

    private fun setTeamSecondRecycler(
        teamSecondRecycler: RecyclerView,
        eachActionList: Summary,
    ) {
        val teamSecondAdapter = TeamSecondRecycler(eachActionList)
        teamSecondRecycler.adapter = teamSecondAdapter
        teamSecondRecycler.layoutManager = LinearLayoutManager(teamSecondRecycler.context)
        teamSecondRecycler.setHasFixedSize(true)
    }

    fun setData(actionsList: MutableList<Summary>) {
        this.actionsList.clear()
        this.actionsList.addAll(actionsList)
        notifyDataSetChanged()
    }
}