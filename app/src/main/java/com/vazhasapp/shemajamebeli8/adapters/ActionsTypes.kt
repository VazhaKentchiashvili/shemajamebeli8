package com.vazhasapp.shemajamebeli8.adapters

enum class ActionsTypes(var actionsTypes: Int) {

    GOAL(1),
    YELLOW_CARD(2),
    RED_CARD(3),
    SUBSTITUTION(4),
}

