package com.vazhasapp.shemajamebeli8.adapters

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vazhasapp.shemajamebeli8.R
import com.vazhasapp.shemajamebeli8.databinding.Team2ActionCardViewBinding
import com.vazhasapp.shemajamebeli8.extensions.setupIcon
import com.vazhasapp.shemajamebeli8.model.Summary
import com.vazhasapp.shemajamebeli8.model.TeamAction

class TeamSecondRecycler(private val secondTeamActions: Summary) :
    RecyclerView.Adapter<TeamSecondRecycler.TeamSecondViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        TeamSecondViewHolder(
            Team2ActionCardViewBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: TeamSecondViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount() = secondTeamActions.team2Action?.size ?: 0

    inner class TeamSecondViewHolder(private val binding: Team2ActionCardViewBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind() {
            val secondTeam: TeamAction? = secondTeamActions.team2Action?.get(adapterPosition)

            binding.tvTeam2PlayerTitle.text = secondTeam?.action?.player1?.playerName
            binding.imTeam2PlayerIcon.setupIcon(secondTeam?.action?.player1?.playerImage.toString())

            when (secondTeam?.actionType) {
                ActionsTypes.GOAL.actionsTypes -> {
                    when (secondTeam.action?.goalType) {
                        GoalTypes.GOAL.goalTypes -> {
                            binding.imTeam2ActionIcon.setImageResource(R.drawable.ic_goal)
                            binding.tvTeam2ActionTime.text = secondTeamActions.actionTime
                            binding.tvTeam2ActionType.text = "${R.string.goals_by}"
                            binding.tvTeam2ActionType.text = "` Goals by"
                        }
                        GoalTypes.OWN_GOAL.goalTypes -> {
                            binding.imTeam2ActionIcon.setImageResource(R.drawable.ic_own_goal)
                            binding.tvTeam2ActionTime.text = secondTeamActions.actionTime
                            binding.tvTeam2ActionTime.setTextColor(Color.RED)
                            binding.tvTeam2ActionType.text = "` Own Goal"
                            binding.tvTeam2ActionType.setTextColor(Color.RED)
                        }
                    }
                }
                ActionsTypes.YELLOW_CARD.actionsTypes -> {
                    binding.imTeam2ActionIcon.setImageResource(R.drawable.ic_yellow_card)
                    binding.tvTeam2ActionTime.text = secondTeamActions.actionTime
                    binding.tvTeam2ActionType.text = "` Tripping"
                }

                ActionsTypes.RED_CARD.actionsTypes -> {
                    binding.imTeam2ActionIcon.setImageResource(R.drawable.ic_yellow_card)
                    binding.imTeam2ActionIcon.setColorFilter(Color.RED)
                    binding.tvTeam2ActionTime.text = secondTeamActions.actionTime
                    binding.tvTeam2ActionType.text = "` Tripping"
                }

                ActionsTypes.SUBSTITUTION.actionsTypes -> {
                    binding.imTeam2ActionIcon.setImageResource(R.drawable.ic_substitution_in)
                    binding.imTeam2ActionIcon2.setImageResource(R.drawable.ic_substitutin_out)
                    binding.tvTeam2ActionTime.text = secondTeamActions.actionTime
                    binding.tvTeam2ActionType.text = "` Substitution"
                    binding.imTeam2PlayerIcon2.visibility = View.VISIBLE
                    binding.imTeam2PlayerIcon2.setImageResource(R.drawable.ic_not_found)
                    binding.tvTeam2PlayerTitle2.text = secondTeam.action?.player2?.playerName
                    binding.tvTeam2PlayerTitle2.visibility = View.VISIBLE
                }
            }
        }
    }
}