package com.vazhasapp.shemajamebeli8.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.vazhasapp.shemajamebeli8.api.FootbalApiService
import com.vazhasapp.shemajamebeli8.model.Football
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MatchViewModel : ViewModel() {

    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO

    private val _footballMatches = MutableLiveData<Football>()
    val footballMatches: LiveData<Football> get() = _footballMatches

    fun init() {
        getFootballMatchs()

    }

    private fun getFootballMatchs() {
        viewModelScope.launch {
            withContext(ioDispatcher) {
                val result = FootbalApiService.footballApiService.footballResult()
                if (result.isSuccessful) {
                    _footballMatches.postValue(result.body())
                }
            }
        }
    }
}