package com.vazhasapp.shemajamebeli8

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.vazhasapp.shemajamebeli8.adapters.MainRecyclerView
import com.vazhasapp.shemajamebeli8.databinding.ActivityMainBinding
import com.vazhasapp.shemajamebeli8.extensions.setupBadge
import com.vazhasapp.shemajamebeli8.extensions.setupIcon
import com.vazhasapp.shemajamebeli8.viewmodel.MatchViewModel
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity() {

    private lateinit var mNavController: NavController
    private lateinit var binding: ActivityMainBinding
    private val myAdapter = MainRecyclerView()

    private val mViewModel: MatchViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupBottomNav()

        init()
    }

    private fun init() {
        mViewModel.init()
        listeners()
        setupRecyclerView()
    }

    private fun listeners() {
        mViewModel.footballMatches.observe(this, {

            myAdapter.setData(it.match!!.matchSummary?.summaries!!.toMutableList())

            it.match.team1?.let { teamOne ->
                setupTeamOne(
                    teamOne.teamImage!!,
                    teamOne.teamName!!,
                    teamOne.score!!
                )
            }
            it.match.team2?.let { teamSecond ->
                setupTeamSecond(
                    teamSecond.teamImage!!,
                    teamSecond.teamName!!,
                    teamSecond.score!!
                )
            }

            binding.tvMatchDate.text = dateConverter(it.match.matchDate!!)
            setupBallPossessions(it.match.team1?.ballPosition!!, it.match.team2?.ballPosition!!)
            setupMatchTimeAndAddress(it.match.matchTime!!.toFloat(), it.match.stadiumAdress!!)
        })
    }

    private fun setupTeamOne(
        team1Icon: String,
        team1Name: String,
        score: Int,
    ) {
        binding.tvTeam1Title.text = team1Name
        binding.imTeam1.setupIcon(team1Icon)
        binding.tvTeam1Score.text = score.toString()
    }

    private fun setupTeamSecond(
        team2Icon: String,
        team2Name: String,
        score: Int,
    ) {
        binding.tvTeam2Title.text = team2Name
        binding.imTeam2.setupIcon(team2Icon)
        binding.tvTeam2Score.text = score.toString()
    }

    private fun setupRecyclerView() {
        binding.mainRecyclerView.apply {
            adapter = myAdapter
            layoutManager = LinearLayoutManager(this@MainActivity)
            setHasFixedSize(true)
        }
    }

    private fun setupBallPossessions(team1Possession: Int, team2Possession: Int) {
        binding.tvTeam1BallPossession.text = team1Possession.toString()
        binding.tvTeam2BallPossession.text = team2Possession.toString()
        binding.progressBar.progress = team1Possession
    }

    private fun setupMatchTimeAndAddress(matchTime: Float, matchAddress: String) {
        binding.tvMatchTime.text = matchTime.toString()
        binding.tvMatchAddress.text = matchAddress
    }

    private fun dateConverter(milliseconds: Long): String {
        val formatter = SimpleDateFormat("dd/MMMM/yyyy");
        return formatter.format(Date(milliseconds))
    }

    private fun setupBottomNav() {
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.navHostFragment) as NavHostFragment
        mNavController = navHostFragment.navController

        binding.bottomNav.setupWithNavController(
            mNavController
        )
        binding.bottomNav.setupBadge(R.id.menuFavorites, 3)
    }
}