package com.vazhasapp.shemajamebeli8.model

data class Football(
    val match: Match?,
    val resultCode: Int?
)