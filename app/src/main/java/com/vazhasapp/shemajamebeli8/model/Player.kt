package com.vazhasapp.shemajamebeli8.model

data class Player(
    val playerImage: String?,
    val playerName: String?
)