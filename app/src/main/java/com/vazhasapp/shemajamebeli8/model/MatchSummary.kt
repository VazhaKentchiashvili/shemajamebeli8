package com.vazhasapp.shemajamebeli8.model

data class MatchSummary(
    val summaries: List<Summary>?
)