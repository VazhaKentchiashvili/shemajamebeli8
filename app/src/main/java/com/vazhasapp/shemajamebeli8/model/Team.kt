package com.vazhasapp.shemajamebeli8.model

data class Team(
    val ballPosition: Int?,
    val score: Int?,
    val teamImage: String?,
    val teamName: String?
)