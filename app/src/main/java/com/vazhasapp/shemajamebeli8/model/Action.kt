package com.vazhasapp.shemajamebeli8.model

import com.google.gson.annotations.SerializedName

data class Action(
    val goalType: Int?,
    @SerializedName("player", alternate = ["player1"])
    val player1: Player?,
    val player2: Player?
)