package com.vazhasapp.shemajamebeli8.model

data class TeamAction(
    val action: Action?,
    val actionType: Int?,
    val teamType: Int?
)